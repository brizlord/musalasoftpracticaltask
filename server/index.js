const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors({
    origin: '*'
}));
app.use(express.urlencoded({extended:false}));

var db=require('./db/index');
app.use(require('./routes/index'));

app.set('port', process.env.PORT || 3000);

app.use(express.json());

app.listen(app.get('port'),()=>{
    console.log(`Server listening on port ${app.get('port')}`);
});