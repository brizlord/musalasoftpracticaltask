const { Router } = require('express');
const router = Router();
const gateways = require('../db/index').gateways;
const peripherals = require('../db/index').peripherals;
var commonFunc = require('../functions/commonFunc');

router.get('/', (req, res) => {
    res.json(
        gateways.data
    );
})

router.get('/gateway/:id', (req, res) => {
    const gatewayId = req.params.id;
    const peripheral_list = commonFunc.findPeripheralInGateway(peripherals, 'gatewayId', gatewayId);

    res.json(
        peripheral_list
    );
})

router.post('/gateway/add', (req, res) => {
    if (req.body.srl && req.body.nam && req.body.ip) {
        const newGateway = {
            'srl': req.body.srl,
            'nam': req.body.nam,
            'ip': req.body.ip
        }
        const isValidIP = commonFunc.validateIPvQtro(newGateway.ip);

        if (isValidIP) {
            const doc = gateways.insert(newGateway);

            if (doc) {
                res.status(200).json(
                    "Inserted gateway"
                );
            }
            else {
                res.status(500).json(
                    "Error Inserting gateway"
                );
            }
        }
        else {
            res.status(500).json(
                "Invalid IP address"
            );
        }
    }
    else {
        res.status(500).json(
            "Empty request"
        );
    }
})

router.post('/peripheral/add', (req, res) => {
    if (req.body.uid && req.body.vendor && req.body.status && req.body.gtwId) {
        const newPeripheral = {
            'uid': req.body.uid,
            'vendor': req.body.vendor,
            'status': req.body.status,
            'gatewayId': req.body.gtwId
        }

        const peripheral_list = commonFunc.findPeripheralInGateway(peripherals, 'gatewayId', newPeripheral.gatewayId);

        if (peripheral_list.length < 10) {
            const doc = peripherals.insert(newPeripheral);

            if (doc) {
                res.status(200).json(
                    "Inserted peripheral"
                );
            }
            else {
                res.status(500).json(
                    "Error Inserting peripheral"
                );
            }
        }
        else {
            res.status(403).json(
                "Maximum devices per gateway"
            );
        }
    } else {
        res.status(500).json(
            "Empty request"
        );
    }
})

router.delete('/peripheral/delete/:id', (req, res) => {
    const id = req.params.id;
    const peripheral = commonFunc.findPeripheralInGateway(peripherals, 'uid', id);

    try {
        peripherals.remove(peripheral);

        res.status(200).json(
            "Peripheral removed"
        );
    }
    catch (exc) {
        res.status(500).json(
            "Error removing peripheral"
        );
    }
});

module.exports = router;