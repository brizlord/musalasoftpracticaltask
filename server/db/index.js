var loki = require('lokijs');
var db = new loki('DBMusalaTest');

const gateways = db.addCollection('gateways');
const peripherals = db.addCollection('peripherals');

module.exports = {gateways,peripherals}
