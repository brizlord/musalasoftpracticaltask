var request = require('request');
var assert = require('assert');

describe('Inserting Gateway', function () {
    const gateway = {
        'srl': '16468894',
        'nam': 'LivingRoom',
        'ip': '192.168.8.208'
    }

    it('should return "Inserted gateway"', async () => {
        request.post({ url: 'http://localhost:3000/gateway/add', form: gateway }, (err, httpResponse, body) => {

            assert.equal(body, '"Inserted gateway"', "Inserted gateway");
        });
    });
});

describe('Inserting Peripheral', function () {
    const peripheral = {
        'uid': '000002222',
        'vendor': 'HP Cam',
        'status': 'Online',
        'gtwId': '16468894'
    }

    it('should return "Inserted peripheral"', async () => {
        request.post({ url: 'http://localhost:3000/peripheral/add', form: peripheral }, (err, httpResponse, body) => {

            assert.equal(body, '"Inserted peripheral"', "Inserted peripheral");
        });
    });
});

describe('Checking maximum amount of peripheral', function () {
    it('should return "Maximum devices per gateway" with more than 10 peripheral', async () => {
        let it = 0;
        while (it < 10) {
            let id = Math.random() * 10000;

            let peripheral = {
                'uid': id,
                'vendor': 'HP Cam',
                'status': 'Online',
                'gtwId': '16468894'
            }

            request.post({ url: 'http://localhost:3000/peripheral/add', form: peripheral }, (err, httpResponse, body) => {
                if (body == '"Maximum devices per gateway"')
                    assert.equal(body, '"Maximum devices per gateway"', "Maximum devices per gateway");
            });

            it++;
        }

    });
});

describe('Getting peripheral by gateway', function () {
    it('should return a list of peripheral', async () => {
        let id = '16468894';

        request.get('http://localhost:3000/gateway/' + id, (err, httpResponse, body) => {
            assert.equal((Array.isArray(JSON.parse(body)) && body.length > 0), true, "Peripheral by gateways");
        });
    });
});

describe('Remove peripheral', function () {
    it('should return "Peripheral removed"', async () => {
        let id = '000002222';

        request.delete('http://localhost:3000/peripheral/delete/' + id, (err, httpResponse, body) => {
            assert.equal(body, '"Peripheral removed"', "Peripheral removed");
        });
    });
});

describe('Invalid IP address', function () {

    it('should return "Invalid IP address" for [d192.168.8.208]', async () => {
        const gateway = {
            'srl': '16468894',
            'nam': 'LivingRoom',
            'ip': 'd192.168.8.208'
        }

        request.post({ url: 'http://localhost:3000/gateway/add', form: gateway }, (err, httpResponse, body) => {

            assert.equal(body, '"Invalid IP address"', "Invalid IP address");
        });
    });

    it('should return "Invalid IP address" for [fghfg.fgh.fgh.fgh]', async () => {
        const gateway = {
            'srl': '16468894',
            'nam': 'LivingRoom',
            'ip': 'fghfg.fgh.fgh.fgh'
        }

        request.post({ url: 'http://localhost:3000/gateway/add', form: gateway }, (err, httpResponse, body) => {

            assert.equal(body, '"Invalid IP address"', "Invalid IP address");
        });
    });

    it('should return "Invalid IP address" for [klvbkcl6564556]', async () => {
        const gateway = {
            'srl': '16468894',
            'nam': 'LivingRoom',
            'ip': 'klvbkcl6564556'
        }

        request.post({ url: 'http://localhost:3000/gateway/add', form: gateway }, (err, httpResponse, body) => {

            assert.equal(body, '"Invalid IP address"', "Invalid IP address");
        });
    });

    it('should return "Invalid IP address" for [10.2.5]', async () => {
        const gateway = {
            'srl': '16468894',
            'nam': 'LivingRoom',
            'ip': '10.2.5'
        }

        request.post({ url: 'http://localhost:3000/gateway/add', form: gateway }, (err, httpResponse, body) => {

            assert.equal(body, '"Invalid IP address"', "Invalid IP address");
        });
    });
});