module.exports = {
    findPeripheralInGateway: function (collection,param, id) {
        return collection.find({ [param]: { '$eq': id } });
    },
    validateIPvQtro: function (ip) {
        const segments = ip.trim().split('.');
        let isvalid=true;

        if (segments.length == 4) {
            for(let value of segments){
                const parsed_value = parseInt(value);

                if (!(parsed_value >= 0 && parsed_value <= 255)) {
                    isvalid=false;
                    break;
                }
            }
        }
        else {
            return false;
        }

        return isvalid;
    }
}