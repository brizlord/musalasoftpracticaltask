import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IDevice } from '../interfaces/idevice';
import { IGateway } from '../interfaces/igateway';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private url: string = "http://localhost:3000";
  private headers: HttpHeaders;
  private gatewaysBS: BehaviorSubject<any>;
  private devicesBS: BehaviorSubject<any>;
  private responseSubject: Subject<any>;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    this.gatewaysBS = new BehaviorSubject<any[]>([]);
    this.devicesBS = new BehaviorSubject<any[]>([]);
    this.responseSubject = new Subject<any>();

    this.retrieveGateways();
  }

  private retrieveGateways() {
    this.http.get(this.url)
      .subscribe(res => {
        this.gatewaysBS.next(res);
      });
  }

  getGateways(): Observable<any[]> {
    return this.gatewaysBS.asObservable();
  }

  addGateway(gateway: IGateway) {
    const body = new HttpParams()
      .set('srl', gateway.srl)
      .set('nam', gateway.nam)
      .set('ip', gateway.ip);

    this.http.post(this.url + '/gateway/add', body.toString(), { headers: this.headers })
      .subscribe(res => {
        this.responseSubject.next(res);
        this.retrieveGateways();
      }, err => {
        this.responseSubject.next(err.error);
      });
  }

  retrieveDevices(gateID: string) {
    this.http.get<IDevice[]>(this.url + '/gateway/' + gateID)
      .subscribe(res => {
        this.devicesBS.next(res);
      });
  }

  getDevices(): Observable<IDevice[]> {
    return this.devicesBS.asObservable();
  }

  getResponse(): Observable<any> {
    return this.responseSubject.asObservable();
  }

  addDevice(device: IDevice) {
    const body = new HttpParams()
      .set('uid', device.uid)
      .set('vendor', device.vendor)
      .set('status', device.status)
      .set('gtwId', device.gtwId);

    this.http.post(this.url + '/peripheral/add', body.toString(), { headers: this.headers })
      .subscribe(res => {
        this.responseSubject.next(res);
        this.retrieveDevices(device.gtwId);
      }, err => {
        this.responseSubject.next(err.error);
      });
  }

  deleteDevice(device:IDevice){
    return this.http.delete(this.url+'/peripheral/delete/'+device.uid);
  }
}
