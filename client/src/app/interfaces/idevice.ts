export interface IDevice {
    uid: string;
    vendor: string;
    status: Status;
    gtwId: string;
    meta?: any;
}

export enum Status {
    Online = 'Online',
    Offline = 'Offline'
}
