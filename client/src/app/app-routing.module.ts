import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GatewayViewComponent } from './subcomponents/gateway-view/gateway-view.component';
import { GatewaysComponent } from './subcomponents/gateways/gateways.component';

const routes: Routes = [
  {
    path:'',
    component:GatewaysComponent
  },
  {
    path:'gatewayView',
    component:GatewayViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
