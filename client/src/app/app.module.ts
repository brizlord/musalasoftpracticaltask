import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GatewaysComponent } from './subcomponents/gateways/gateways.component';
import { HttpClientModule } from '@angular/common/http';
import { AddGatewayComponent } from './subcomponents/add-gateway/add-gateway.component';
import { FormsModule } from '@angular/forms';
import { GatewayViewComponent } from './subcomponents/gateway-view/gateway-view.component';
import { AddDeviceComponent } from './subcomponents/add-device/add-device.component';

@NgModule({
  declarations: [
    AppComponent,
    GatewaysComponent,
    AddGatewayComponent,
    GatewayViewComponent,
    AddDeviceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
