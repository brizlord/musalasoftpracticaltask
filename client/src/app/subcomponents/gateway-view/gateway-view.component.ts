import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IDevice } from 'src/app/interfaces/idevice';
import { IGateway } from 'src/app/interfaces/igateway';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-gateway-view',
  templateUrl: './gateway-view.component.html',
  styleUrls: ['./gateway-view.component.scss']
})
export class GatewayViewComponent implements OnInit {
  gateway: IGateway | any;
  gateways: IGateway[] = [];
  devices: IDevice[] = [];
  clickedAddDevice: boolean = false;
  responseError: string = '';

  constructor(private router: Router, private dataSvc: DataService) {
    if (router.getCurrentNavigation()?.extras) {
      this.gateway = router.getCurrentNavigation()?.extras;

      if (this.gateway.state) {
        this.gateway = this.gateway.state['gatew'];
      }

      if (!this.gateway) {
        this.router.navigateByUrl('');
      }
    }
  }

  ngOnInit(): void {
    if(this.gateway){
      this.dataSvc.retrieveDevices(this.gateway.srl);
    }

    this.dataSvc.getGateways()
      .subscribe(gates => {
        this.gateways = gates;

        if (this.gateway) {
          this.gatewayInfo(this.gateway);
        }
      })

    this.dataSvc.getDevices()
      .subscribe(devices => {
        this.devices = devices;
      });
  }

  gatewayInfo(gate: IGateway) {
    this.reverseActivatedClass();

    const gateelement: HTMLElement | null = document.getElementById(gate.srl);

    if (gateelement) {
      gateelement.classList.add('activated');

      this.gateway = gate;
      this.dataSvc.retrieveDevices(this.gateway.srl);
    }
  }

  reverseActivatedClass() {
    const collection: HTMLCollection = document.getElementsByClassName('activated');

    for (let i = 0; i < collection.length; i++) {
      collection.item(i)?.classList.remove('activated');
    }

    this.devices = [];
  }

  addDevice() {
    this.clickedAddDevice = true;
  }

  deleteDevice(device: IDevice) {
    this.dataSvc.deleteDevice(device)
      .subscribe(res => {
        this.dataSvc.retrieveDevices(this.gateway.srl);
      },
        err => {
          this.responseError = err.error;
        });
  }

  addGateway(){
    this.router.navigateByUrl('');
  }

  parseDate(timestamp:string){
    return new Date(timestamp).toDateString();
  }
}
