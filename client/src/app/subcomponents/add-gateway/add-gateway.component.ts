import { Component, OnInit } from '@angular/core';
import { IGateway } from 'src/app/interfaces/igateway';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-gateway',
  templateUrl: './add-gateway.component.html',
  styleUrls: ['./add-gateway.component.scss']
})
export class AddGatewayComponent implements OnInit {
  gateway: IGateway;
  response: string;

  constructor(private dataSvc: DataService) {
    this.gateway = {
      srl: "",
      nam: "",
      ip: ""
    };

    this.response = "";
  }

  ngOnInit(): void {
    this.dataSvc.getResponse()
      .subscribe(res => {
        this.response = res;
      });
  }

  addGateway() {
    this.dataSvc.addGateway(this.gateway);
  }
}
