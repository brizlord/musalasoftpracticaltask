import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IDevice, Status } from 'src/app/interfaces/idevice';
import { IGateway } from 'src/app/interfaces/igateway';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.scss']
})
export class AddDeviceComponent implements OnInit,OnChanges {
  @Input() gateway: IGateway = {
    srl: "",
    nam: "",
    ip: ""
  };
  device: IDevice;
  response: string;

  constructor(private dataSvc: DataService) {
    this.device = {
      uid: "",
      vendor: "",
      status: Status.Offline,
      gtwId: ''
    };

    this.response = "";
  }

  ngOnInit(): void {
    this.dataSvc.getResponse()
      .subscribe(res => {
        this.response = res;
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.device.gtwId = this.gateway.srl;
    this.response='';
  }

  addDevice() {
    this.dataSvc.addDevice(this.device);
  }
}
