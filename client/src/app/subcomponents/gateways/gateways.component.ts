import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IGateway } from 'src/app/interfaces/igateway';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-gateways',
  templateUrl: './gateways.component.html',
  styleUrls: ['./gateways.component.scss']
})
export class GatewaysComponent implements OnInit {
  gateways: any[];
  clickedAddGateway: boolean;

  constructor(private dataSvc: DataService, private router: Router) {
    this.gateways = [];
    this.clickedAddGateway = false;
  }

  ngOnInit(): void {
    this.dataSvc.getGateways()
      .subscribe(res => {
        this.gateways = res;
      });
  }

  addGateway() {
    this.clickedAddGateway = true;
  }

  openGateway(gateway: IGateway) {
    this.router.navigateByUrl('gatewayView', {
      state: {
        gatew: gateway
      }
    })
  }

}
