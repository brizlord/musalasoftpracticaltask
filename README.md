# Client

Developed with Angular
##
Start client app with the command ng serve

# Server

Developed with NodeJS
##
Run server with command node index.js

# Unique Command

From client app directory run command npm run start to start client and server in parallel

